from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Ciudades(models.Model):
    nombre = models.CharField(max_length=100)
    pais = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

class Estadios(models.Model):
    nombre = models.CharField(max_length=100)
    ciudad = models.ForeignKey(Ciudades, on_delete=models.CASCADE)
    capacidad = models.PositiveIntegerField()

    def __str__(self):
        return self.nombre



class Propietarios(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)


    def __str__(self):
        return f"{self.nombre} {self.apellido}"


class Jugadores(models.Model):
    nombre = models.CharField(max_length=100)
    numero = models.IntegerField()
    posicion = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

class Equipos(models.Model):
    nombre = models.CharField(max_length=100)
    liga = models.CharField(max_length=100)
    fundacion = models.DateField()
    ciudad = models.ForeignKey(Ciudades, on_delete=models.CASCADE)
    propietario = models.ForeignKey(Propietarios, on_delete=models.CASCADE, related_name='equipos_del_propietario')
    jugadores = models.ManyToManyField(Jugadores)

    def __str__(self):
        return self.nombre


class PropietariosEstadios(models.Model):
    propietario = models.OneToOneField(Propietarios, on_delete=models.CASCADE)
    estadio = models.OneToOneField(Estadios, on_delete=models.CASCADE)

    def __str__(self):
        return f"Estadio de {self.propietario.nombre} {self.propietario.apellido}: {self.estadio.nombre}"
