from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import Ciudades, Estadios, Propietarios, Jugadores, Equipos, PropietariosEstadios
from .forms import (
    CiudadesForm, UpdateCiudadesForm, EquiposForm, UpdateEquiposForm,
    EstadiosForm, UpdateEstadiosForm, PropietariosForm, UpdatePropietariosForm,
    JugadoresForm, UpdateJugadoresForm, PropietariosEstadiosForm, UpdatePropietariosEstadiosForm,
)

# Views for Ciudades
class CreateCiudades(generic.CreateView):
    template_name = "core/ciudades/create_ciudades.html"
    model = Ciudades
    form_class = CiudadesForm
    success_url = reverse_lazy("core:list_ciudades")

class ListCiudades(generic.View):
    template_name = "core/ciudades/list_ciudades.html"
    context = {}

    def get(self, request, *args, **kwargs):
        ciudades = Ciudades.objects.all()
        self.context = {
            "ciudades": ciudades
        }
        return render(request, self.template_name, self.context)

class DetailCiudades(generic.View):
    template_name = "core/ciudades/detail_ciudades.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        ciudad = Ciudades.objects.get(pk=pk)
        self.context = {
            "ciudad": ciudad
        }
        return render(request, self.template_name, self.context)

class UpdateCiudades(generic.UpdateView):
    template_name = "core/ciudades/update_ciudades.html"
    model = Ciudades
    form_class = UpdateCiudadesForm
    success_url = reverse_lazy("core:list_ciudades")

class DeleteCiudades(generic.DeleteView):
    template_name = "core/ciudades/delete_ciudades.html"
    model = Ciudades
    success_url = reverse_lazy("core:list_ciudades")

# Views for Equipos
class CrearEquipo(generic.CreateView):
    template_name = "core/equipos/create_equipos.html"
    model = Equipos
    form_class = EquiposForm
    success_url = reverse_lazy("core:list_equipos")

class ListarEquipos(generic.View):
    template_name = "core/equipos/list_equipos.html"
    context = {}

    def get(self, request, *args, **kwargs):
        equipos = Equipos.objects.all()
        self.context = {
            "equipos": equipos
        }
        return render(request, self.template_name, self.context)

class DetalleEquipo(generic.View):
    template_name = "core/equipos/detail_equipos.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        equipo = Equipos.objects.get(pk=pk)
        self.context = {
            "equipo": equipo
        }
        return render(request, self.template_name, self.context)

class ActualizarEquipo(generic.UpdateView):
    template_name = "core/equipos/update_equipos.html"
    model = Equipos
    form_class = UpdateEquiposForm
    success_url = reverse_lazy("core:list_equipos")

class EliminarEquipo(generic.DeleteView):
    template_name = "core/equipos/delete_equipos.html"
    model = Equipos
    success_url = reverse_lazy("core:list_equipos")

# Views for Estadios
class CrearEstadio(generic.CreateView):
    template_name = "core/estadios/create_estadios.html"
    model = Estadios
    form_class = EstadiosForm
    success_url = reverse_lazy("core:list_estadios")

class ListarEstadios(generic.View):
    template_name = "core/estadios/list_estadios.html"
    context = {}

    def get(self, request, *args, **kwargs):
        estadios = Estadios.objects.all()
        self.context = {
            "estadios": estadios
        }
        return render(request, self.template_name, self.context)

class DetalleEstadio(generic.View):
    template_name = "core/estadios/detail_estadios.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        estadio = Estadios.objects.get(pk=pk)
        self.context = {
            "estadio": estadio
        }
        return render(request, self.template_name, self.context)

class ActualizarEstadio(generic.UpdateView):
    template_name = "core/estadios/update_estadios.html"
    model = Estadios
    form_class = UpdateEstadiosForm
    success_url = reverse_lazy("core:list_estadios")

class EliminarEstadio(generic.DeleteView):
    template_name = "core/estadios/delete_estadios.html"
    model = Estadios
    success_url = reverse_lazy("core:list_estadios")

# Views for Propietarios
class CrearPropietario(generic.CreateView):
    template_name = "core/propietarios/create_propietarios.html"
    model = Propietarios
    form_class = PropietariosForm
    success_url = reverse_lazy("core:list_propietarios")

class ListarPropietarios(generic.View):
    template_name = "core/propietarios/list_propietarios.html"
    context = {}

    def get(self, request, *args, **kwargs):
        propietarios = Propietarios.objects.all()
        self.context = {
            "propietarios": propietarios
        }
        return render(request, "core/propietarios/list_propietarios.html", self.context)

class DetallePropietario(generic.View):
    template_name = "core/propietarios/detail_propietarios.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        propietario = Propietarios.objects.get(pk=pk)
        self.context = {
            "propietario": propietario
        }
        return render(request, "core/propietarios/detail_propietarios.html", self.context)

class ActualizarPropietario(generic.UpdateView):
    template_name = "core/propietarios/update_propietarios.html"
    model = Propietarios
    form_class = UpdatePropietariosForm
    success_url = reverse_lazy("core:list_propietarios")

class EliminarPropietario(generic.DeleteView):
    template_name = "core/propietarios/delete_propietarios.html"
    model = Propietarios
    success_url = reverse_lazy("core:list_propietarios")

# Views for Jugadores
class CrearJugador(generic.CreateView):
    template_name = "core/jugadores/create_jugadores.html"
    model = Jugadores
    form_class = JugadoresForm
    success_url = reverse_lazy("core:list_jugadores")

class ListarJugadores(generic.View):
    template_name = "core/jugadores/list_jugadores.html"
    context = {}

    def get(self, request, *args, **kwargs):
        jugadores = Jugadores.objects.all()
        self.context = {
            "jugadores": jugadores
        }
        return render(request, "core/jugadores/list_jugadores.html", self.context)

class DetalleJugador(generic.View):
    template_name = "core/jugadores/detail_jugadores.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        jugador = Jugadores.objects.get(pk=pk)
        self.context = {
            "jugador": jugador
        }
        return render(request, "core/jugadores/detail_jugadores.html", self.context)

class ActualizarJugador(generic.UpdateView):
    template_name = "core/jugadores/update_jugadores.html"
    model = Jugadores
    form_class = UpdateJugadoresForm
    success_url = reverse_lazy("core:list_jugadores")

class EliminarJugador(generic.DeleteView):
    template_name = "core/jugadores/delete_jugadores.html"
    model = Jugadores
    success_url = reverse_lazy("core:list_jugadores")


# Views for PropietariosEstadios
class CrearPropietarioEstadio(generic.CreateView):
    template_name = "core/propiest/create_propietario_estadio.html"
    model = PropietariosEstadios
    form_class = PropietariosEstadiosForm
    success_url = reverse_lazy("core:list_propietario_estadios")

class ListarPropietarioEstadios(generic.View):
    template_name = "core/propiest/list_propietario_estadios.html"
    context = {}

    def get(self, request, *args, **kwargs):
        propietarios_estadios = PropietariosEstadios.objects.all()
        self.context = {
            "propietarios_estadios": propietarios_estadios
        }
        return render(request, "core/propiest/list_propietario_estadios.html", self.context)

class DetallePropietarioEstadio(generic.View):
    template_name = "core/propiest/detail_propietario_estadio.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        propietario_estadio = PropietariosEstadios.objects.get(pk=pk)
        self.context = {
            "propietario_estadio": propietario_estadio
        }
        return render(request, "core/propiest/detail_propietario_estadios.html", self.context)

class ActualizarPropietarioEstadio(generic.UpdateView):
    template_name = "core/propiest/update_propietario_estadio.html"
    model = PropietariosEstadios
    form_class = UpdatePropietariosEstadiosForm
    success_url = reverse_lazy("core:list_propietario_estadios")

class EliminarPropietarioEstadio(generic.DeleteView):
    template_name = "core/propiest/delete_propietario_estadio.html"
    model = PropietariosEstadios
    success_url = reverse_lazy("core:list_propietario_estadios")
