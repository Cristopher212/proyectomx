from django.contrib import admin



from django.contrib import admin
from .models import Ciudades, Estadios, Propietarios, Jugadores, Equipos, PropietariosEstadios
# Register your models here.

@admin.register(Ciudades)
class CiudadesAdmin(admin.ModelAdmin):
    list_display = [
        "nombre", 
        "pais"]

@admin.register(Estadios)
class EstadiosAdmin(admin.ModelAdmin):
    list_display = [
        "nombre", 
        "ciudad", 
        "capacidad"]

@admin.register(Propietarios)
class PropietariosAdmin(admin.ModelAdmin):
    list_display = [
        "nombre"]

@admin.register(Jugadores)
class JugadoresAdmin(admin.ModelAdmin):
    list_display = [
        "nombre", 
        "numero", 
        "posicion"]

@admin.register(Equipos)
class EquiposAdmin(admin.ModelAdmin):
    list_display = [
        "nombre", 
        "liga", 
        "fundacion", 
        "ciudad", 
        "propietario"]

@admin.register(PropietariosEstadios)
class PropietariosEstadiosAdmin(admin.ModelAdmin):
    list_display = [
        "propietario", 
        "estadio"]
