from django import forms
from .models import Ciudades, Estadios, Propietarios, Jugadores, Equipos, PropietariosEstadios

### C I U D A D E S

class CiudadesForm(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre de la ciudad", "style": "background-color: gray;"}),
            "pais": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "País de la ciudad", "style": "background-color: gray;"})
        }


class UpdateCiudadesForm(forms.ModelForm):
    class Meta:
        model = Ciudades
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre de la ciudad", "style": "background-color: gray;"}),
            "pais": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "País de la ciudad", "style": "background-color: gray;" })
        }


### E S T A D I O S

class EstadiosForm(forms.ModelForm):
    class Meta:
        model = Estadios
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del estadio", "style": "background-color: gray;"}),
            "ciudad": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"}),
            "capacidad": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Capacidad del estadio", "style": "background-color: gray;"})
        }

class UpdateEstadiosForm(forms.ModelForm):
    class Meta:
        model = Estadios
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del estadio", "style": "background-color: gray;"}),
            "ciudad": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"}),
            "capacidad": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Capacidad del estadio", "style": "background-color: gray;"})
        }


### P R O P I E T A R I O S

class PropietariosForm(forms.ModelForm):
    class Meta:
        model = Propietarios
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del propietario", "style": "background-color: gray;"}),
            "apellido": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Apellido del propietario", "style": "background-color: gray;"}),
             "equipos": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Apellido del propietario", "style": "background-color: gray;"})
        }

class UpdatePropietariosForm(forms.ModelForm):
    class Meta:
        model = Propietarios
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del propietario", "style": "background-color: gray;"}),
            "apellido": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Apellido del propietario", "style": "background-color: gray;"})
        }


### J U G A D O R E S

class JugadoresForm(forms.ModelForm):
    class Meta:
        model = Jugadores
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del jugador", "style": "background-color: gray;"}),
            "numero": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Número del jugador", "style": "background-color: gray;"}),
            "posicion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Posición del jugador", "style": "background-color: gray;"})
        }

class UpdateJugadoresForm(forms.ModelForm):
    class Meta:
        model = Jugadores
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del jugador", "style": "background-color: gray;"}),
            "numero": forms.NumberInput(attrs={"class": "form-control", "placeholder": "Número del jugador", "style": "background-color: gray;"}),
            "posicion": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Posición del jugador", "style": "background-color: gray;"})
        }


### E Q U I P O S

class EquiposForm(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del equipo", "style": "background-color: gray;"}),
            "liga": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Liga del equipo", "style": "background-color: gray;"}),
            "fundacion": forms.DateInput(attrs={"type": "date", "class": "form-control", "style": "background-color: gray;"}),
            "ciudad": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"}),
            "propietario": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"}),
            "jugadores": forms.SelectMultiple(attrs={"class": "form-control", "style": "background-color: gray;"})
        }

class UpdateEquiposForm(forms.ModelForm):
    class Meta:
        model = Equipos
        fields = '__all__'

        widgets = {
            "nombre": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Nombre del equipo", "style": "background-color: gray;"}),
            "liga": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Liga del equipo", "style": "background-color: gray;"}),
            "fundacion": forms.DateInput(attrs={"type": "date", "class": "form-control", "style": "background-color: gray;"}),
            "ciudad": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"}),
            "propietario": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"}),
            "jugadores": forms.SelectMultiple(attrs={"class": "form-control", "style": "background-color: gray;"})
        }

### P R O E S T
class PropietariosEstadiosForm(forms.ModelForm):
    class Meta:
        model = PropietariosEstadios
        fields = '__all__'

        widgets = {
            "propietario": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"}),
            "estadio": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"})
        }

class UpdatePropietariosEstadiosForm(forms.ModelForm):
    class Meta:
        model = PropietariosEstadios
        fields = '__all__'

        widgets = {
            "propietario": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"}),
            "estadio": forms.Select(attrs={"class": "form-control", "style": "background-color: gray;"})
        }
