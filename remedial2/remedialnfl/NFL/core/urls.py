from django.urls import path
from core import views

app_name = "core"

urlpatterns = [
    #####C I U D A D E S
    path('create/ciudades/', views.CreateCiudades.as_view(), name="create_ciudades"),
    path('list/ciudades/', views.ListCiudades.as_view(), name="list_ciudades"),
    path('detail/ciudades/<int:pk>/', views.DetailCiudades.as_view(), name="detail_ciudades"),
    path('update/ciudades/<int:pk>/', views.UpdateCiudades.as_view(), name="update_ciudades"),
    path('delete/ciudades/<int:pk>/', views.DeleteCiudades.as_view(), name="delete_ciudades"),

    #### E Q U I P O S
    path('create/equipos/', views.CrearEquipo.as_view(), name="create_equipos"),
    path('list/equipos/', views.ListarEquipos.as_view(), name="list_equipos"),
    path('detail/equipos/<int:pk>/', views.DetalleEquipo.as_view(), name="detail_equipos"),
    path('update/equipos/<int:pk>/', views.ActualizarEquipo.as_view(), name="update_equipos"),
    path('delete/equipos/<int:pk>/', views.EliminarEquipo.as_view(), name="delete_equipos"),


    #### E S T  A D I O S
    path('create/estadios/', views.CrearEstadio.as_view(), name="create_estadios"),
    path('list/estadios/', views.ListarEstadios.as_view(), name="list_estadios"),
    path('detail/estadios/<int:pk>/', views.DetalleEstadio.as_view(), name="detail_estadios"),
    path('update/estadios/<int:pk>/', views.ActualizarEstadio.as_view(), name="update_estadios"),
    path('delete/estadios/<int:pk>/', views.EliminarEstadio.as_view(), name="delete_estadios"),


    #### P R O P P I E T A R I O S
    path('create/propietarios/', views.CrearPropietario.as_view(), name="create_propietarios"),
    path('list/propietarios/', views.ListarPropietarios.as_view(), name="list_propietarios"),
    path('detail/propietarios/<int:pk>/', views.DetallePropietario.as_view(), name="detail_propietarios"),
    path('update/propietarios/<int:pk>/', views.ActualizarPropietario.as_view(), name="update_propietarios"),
    path('delete/propietarios/<int:pk>/', views.EliminarPropietario.as_view(), name="delete_propietarios"),


    #### J U G A D O R E S
    path('create/jugadores/', views.CrearJugador.as_view(), name="create_jugadores"),
    path('list/jugadores/', views.ListarJugadores.as_view(), name="list_jugadores"),
    path('detail/jugadores/<int:pk>/', views.DetalleJugador.as_view(), name="detail_jugadores"),
    path('update/jugadores/<int:pk>/', views.ActualizarJugador.as_view(), name="update_jugadores"),
    path('delete/jugadores/<int:pk>/', views.EliminarJugador.as_view(), name="delete_jugadores"),


    ### P R O  E S T    
    path('create/propietario_estadios/', views.CrearPropietarioEstadio.as_view(), name="create_propietario_estadio"),
    path('list/propietario_estadios/', views.ListarPropietarioEstadios.as_view(), name="list_propietario_estadios"),
    path('detail/propietario_estadios/<int:pk>/', views.DetallePropietarioEstadio.as_view(), name="detail_propietario_estadio"),
    path('update/propietario_estadios/<int:pk>/', views.ActualizarPropietarioEstadio.as_view(), name="update_propietario_estadio"),
    path('delete/propietario_estadios/<int:pk>/', views.EliminarPropietarioEstadio.as_view(), name="delete_propietario_estadio"),

]
